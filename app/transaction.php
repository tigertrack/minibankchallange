<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $guarded = [];

    public function customer(){
        return $this->belongTo('App\customer');
    }
}
