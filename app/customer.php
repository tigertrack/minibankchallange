<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $guard = [];

    public function account(){
        return $this->hasOne('App\account');
    }

    public function transaction(){
        return $this->hasMany('App\transaction');
    }
}
